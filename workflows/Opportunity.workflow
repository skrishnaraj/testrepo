<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_To_Primary_contact</fullName>
        <ccEmails>sharath.k@extentor.com</ccEmails>
        <description>Send email To contact</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TGC_Lease_Notification_Template/LeaseExpiryNotificationTemplate</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lease_Enquiry</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Lease_Enquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Lease Enquiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Listing_Opportunity</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Listing_Opportunity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Listing Opportunity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sale_Enquiry</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Sale_Enquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Sale Enquiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Enquiry Category-Lease</fullName>
        <actions>
            <name>Lease_Enquiry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Enquiry_Category__c</field>
            <operation>equals</operation>
            <value>Lease,Sublease</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Enquiry Category-Sale</fullName>
        <actions>
            <name>Sale_Enquiry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Enquiry_Category__c</field>
            <operation>equals</operation>
            <value>Sale</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lease Email Sent Changed</fullName>
        <actions>
            <name>Send_email_To_Primary_contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Lease_Email_Sent__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Listing Opportunity</fullName>
        <actions>
            <name>Listing_Opportunity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Enquiry_Category__c</field>
            <operation>equals</operation>
            <value>Opportunity</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
