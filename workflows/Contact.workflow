<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_To_contact</fullName>
        <ccEmails>developer@extentor.com</ccEmails>
        <description>Send email To contact</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TGC_Lease_Notification_Template/LeaseExpiryNotificationTemplate</template>
    </alerts>
</Workflow>
