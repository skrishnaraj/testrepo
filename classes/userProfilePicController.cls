global with sharing class userProfilePicController {
	
	//veriable declaration
	public Id oppId {get; set;}
	
	//Constructor
	public userProfilePicController(){
		
	}
	
	//return Opprotunity owner profile picture link from document
	public String getUserOppProfilePic(){
		Opportunity opportunityRec = [SELECT OwnerId FROM Opportunity WHERE Id =: oppId Limit 1];
		User userRec = [SELECT Id, Name FROM User WHERE Id =: opportunityRec.OwnerId Limit 1];
		String userName = userRec.Name;
		userName = userName.trim();
		UserProfilePicture__c picCustomSetting = UserProfilePicture__c.getInstance(userName);
        String picLink = picCustomSetting.Profile_Pic_Doc_Link__c;

		return picLink;
    }
}