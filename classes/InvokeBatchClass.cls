global class InvokeBatchClass {
	
	webservice static String executeBatch(List<Opportunity> opps)
    {
		LeaseExpiryNotification runBatch = new LeaseExpiryNotification();
		Database.executeBatch(runBatch);
		return 'success';
    }

}