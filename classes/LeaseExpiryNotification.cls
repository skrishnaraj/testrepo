/* ================================================
@Name:  LeaseExpiryNotification
@Copyright notice: 
Copyright (c) 2014, TGC and developed by Extentor
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are not permitted.                                                                                                    
@====================================================
@====================================================
@Purpose: This batch class will fetch all opportunity of which lease is expiry in next 6 months and it will send notification to customer
@====================================================
@History                                                                                                                    
@---------                                                                                                                       
@VERSION________AUTHOR______________DATE______________DETAIL                   
 1.0        Kishlay@extentor     18/07/2014      INITIAL DEVELOPMENT  
 1.1        Kishlay@extentor	 21/07/2014      Added SingleEmailMessage                  
   
@=======================================================  */
global class LeaseExpiryNotification implements Database.Batchable<sObject>
{
    global String query;
    global date d;
    global LeaseExpiryNotification(){
        d= System.Test.isRunningTest()? d:date.today();
        system.debug(d +'**********');
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    	// getting lease record type id
        id leaseRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Lease Enquiry').getRecordTypeId();
        Template_Settings__c templateCustomSetting = Template_Settings__c.getValues('TGCValues');    
        // query all opportnutiny which is closed won and lease is expiry in next 6 months
        String leaseExpiryDate = String.valueOf(date.Today().addDays(Integer.valueOf(templateCustomSetting.DaysBefore__c)));
        query = 'SELECT ID,Name,Budget__c,Floor_Area__c,Lease_Expiry_Date__c,Location__c,RecordTypeID,Size__c,StageName,CloseDate,AccountId FROM Opportunity WHERE IsWon = true AND Lease_Expiry_Date__c = '+leaseExpiryDate +' AND RecordTypeID =:leaseRecordTypeId'  ;
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope){

        Set<ID> oppIDs = new Set<ID>();
        List<Opportunity> listOfOpps = new List<Opportunity>();
        LIst<OpportunityContactRole> oppContactList = new List<OpportunityContactRole>();
        Map<id,id> oppContactID = new Map<id,id>();
        List<Messaging.SingleEmailMessage> allMails = new List<Messaging.SingleEmailMessage>();
        // getting lease record type of listing 
        id leaseRecordTypeIdOfListing = Schema.SObjectType.Listing__c.getRecordTypeInfosByName().get('Lease Listing').getRecordTypeId();
        

        for(sObject tempOpp : scope) {
             Opportunity opp = (Opportunity)tempOpp;
             oppIDs.add(opp.id);
        } 

        oppContactList = [SELECT ContactId,Id,IsPrimary,OpportunityId,Role FROM OpportunityContactRole WHERE OpportunityId IN: oppIDs AND IsPrimary = true];
        system.debug(oppContactList+'oppContactList##########');
        if(oppContactList.size() > 0){
        	for(OpportunityContactRole tempOCR : oppContactList){
        		Opportunity opp = new Opportunity();
	        	opp.Id = tempOCR.OpportunityId;
	        	opp.Primary_Contact__c = tempOCR.ContactId;
	        	opp.Lease_Email_Sent__c = datetime.now();
	        	listOfOpps.add(opp);
        	}
        }

        
        if(listofOpps.size() > 0){
			database.update(listOfOpps);
        }
        
    }
     
    
    global void finish(Database.BatchableContext BC)
    {
    }
}