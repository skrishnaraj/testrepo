global with sharing class LeaseNotificationVFTempController {
    
    Opportunity opportunityRec = new Opportunity();
    List<Listing__c> listingList = new List<Listing__c>();
    public Id oppId {get; set;}
    public String sfdcURL {get; set;}
    public decimal tolPercent {get; set;}
    public List<Listing__c> collectionListingList = new List<Listing__c>();
    
    public LeaseNotificationVFTempController() {
        sfdcURL = URL.getSalesforceBaseUrl().toExternalForm()+'/';
        
        Template_Settings__c tolCustomSetting = Template_Settings__c.getValues('TGCValues');                
        tolPercent = tolCustomSetting.Tolerance__c;

    }
    
    public List<Listing__c> SearchRelatedListing(Opportunity oppToConsider,List<Listing__c> listingList ) {

        Decimal low,high,lowBud,highBud;
        List<String> numericString = new List<String>();
        List<String> numericStringForBudget = new List<String>();
        List<Listing__c> matchingListings = new List<Listing__c>();
        List<String> sizeList = oppToConsider.Size__c.replaceAll(',','').splitByCharacterType(); // get values as {200,sqm,-,400,sqm}
        List<String> budgetList = oppToConsider.Budget__c.replaceAll(',','').splitByCharacterType();// get values as {$,20000,-,$,40000}
        String queryString = 'SELECT ';
        Set<String> suburbsToConsider = new Set<String>();
        
       
        
        if(oppToConsider.Location__c != null) {
            List<String> locationsToConsider = oppToConsider.Location__c.split(';');
                
            for(LocationRegionMap__c locRegionMapping :[select  OppLocation__c , region__c from LocationRegionMap__c where OppLocation__c IN :locationsToConsider]){
            	suburbsToConsider.add(locRegionMapping.Region__c);
            }
    	}
        
        //system.assertEquals(suburbsToConsider,null);
            
        // for Size
        for(String tempString : sizeList){
            if(tempString.isNumeric()){
                numericString.add(tempString);
            }
            else if(tempString == '<'){
                low = 0;
            }
            
        }
        if(numericString.size() == 2){
            if(low == null){
                low = Integer.valueOf(numericString[0]);
                high = Integer.valueOf(numericString[1]);
            }
            
        }
        if(numericString.size() == 1){
            if(low != null){
                high = Integer.valueOf(numericString[0]);
            }
            else{
                low = Integer.valueOf(numericString[0]);
                high = 10000000;//random large value so that high != null
            }
        }

        
        // for Budget
        for(String tempString : budgetList){
            if(tempString.isNumeric()){
                numericStringForBudget.add(tempString);
            }
            else if(tempString == '<'){
                lowBud = 0;
            }
            
        }
        if(numericStringForBudget.size() == 2){
            if(lowBud == null){
                lowBud = Integer.valueOf(numericStringForBudget[0]);
                highBud = Integer.valueOf(numericStringForBudget[1]);
            }
            
        }
        if(numericStringForBudget.size() == 1){
            if(lowBud != null){
                highBud = Integer.valueOf(numericStringForBudget[0]);
                highBud = highBud + Integer.valueOf(highBud*(tolPercent/100)); 
            }
            else{
                lowBud = Integer.valueOf(numericStringForBudget[0]);
                highBud = 1000000000;//random large value so that high != null
            }
        }
        
        for(Listing__c tempListing : listingList){
          	
                if(((tempListing.Area_m2__c >= low && tempListing.Area_m2__c <= high) || (tempListing.Annual_Rent__c >= lowBud && tempListing.Annual_Rent__c <= highBud)) && suburbsToConsider.contains(tempListing.Region__c)){
                    matchingListings.add(tempListing);
                }

        }  
        return  matchingListings;
    }
    
    public list<Listing__c> getListingRecordList() {
        id leaseRecordTypeIdOfListing = Schema.SObjectType.Listing__c.getRecordTypeInfosByName().get(Constants.listingRecordType).getRecordTypeId();
        opportunityRec = [SELECT Name,Budget__c,Lease_Expiry_Date__c,Size__c,Location__c, OwnerId FROM Opportunity WHERE Id =: oppId Limit 1];
        List<String> sizeList = opportunityRec.Size__c.splitByCharacterType();
        listingList = [SELECT ID,Name,PropertyID__c,Region__c,Suburb__c,Area_m2__c,Property_Image__c,Annual_Rent__c,Summary__c,Level__c,Suite__c,Outgoings_m2__c,RecordTypeId FROM Listing__c WHERE RecordTypeId =: leaseRecordTypeIdOfListing];
        collectionListingList = SearchRelatedListing(opportunityRec,listingList);
        return collectionListingList;
        
    }
}